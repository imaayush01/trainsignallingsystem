# README #

Train Signalling
in list Doing
DescriptionEdit
TRAIN SIGNALLING SYSTEM
We are designing a Train Signalling System to ensure that trains run on the same line smoothly without any collision.
For this, we have divided the space between 2 consecutive stations into multiple blocks where a block signifies the space between 2 consecutive signals.

Station Station
A B
S0 S1 S2 S3
|<--------B0---------------->|<------- B1----------------->|<------ B2----------------->|

In the above diagram, S0, S1, S2, S3 are Signals. Signals S0 and S3 are at Stations A and B respectively.
B0, B1 and B2 are the Blocks.

Assume that there are 2 trains moving from Station A to Station B. They are on the same line; one directly behind the other.
Assume that initially all signals are Green.
Story 1:
The first train moves from S0 to S1. Thus, it now occupies Block B0.
This should turn Signal S0 to Red.
The Red S0 signal will indicate to the lagging train that it has to wait till Block B0 is free.

Story 2:
The first train moves from S1 to S2. Thus, it exits Block B0 and now occupies Block B1.
This should turn:
• Signal S1 to Red
• Signal S0 to Yellow.
The Yellow S0 signal will indicate to the lagging train that the next block (B0 in this case) is free and it can proceed further to that block with caution.

Story 3:
The first train moves from S2 to S3. Thus it exits Block B1 and now occupies Block B2.
This should turn:
• Signal S2 to Red
• Signal S1 to Yellow
• Signal S0 to Double Yellow.
The Double Yellow S0 signal will indicate to the lagging train that the next 2 blocks (B0 and B1) are free and it can safely traverse 2 blocks.

Story 4:
The first train exits Station B. Thus it exits Block B2.
This should turn:
• Signal S3 to Red
• Signal S2 to Yellow
• Signal S1 to Double Yellow
• Signal S0 to Green.
The Green S0 signal indicates to the lagging train that the line from Station A to Station B is clear.

