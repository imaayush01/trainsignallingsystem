package com.oogway.trainsignallings;

import com.oogway.trainsignallingsystem.models.Block;
import com.oogway.trainsignallingsystem.models.Signal;
import com.oogway.trainsignallingsystem.models.Station;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.oogway.trainsignallingsystem.models.Signal.signals.*;
import static junit.framework.TestCase.assertEquals;

public class TrainCrossNextStationTests {

   /* Story 4:
    The first train exits Station B. Thus it exits Block B2.
    This should turn:
    • Signal S3 to Red
    • Signal S2 to Yellow
    • Signal S1 to Double Yellow
    • Signal S0 to Green.
    The Green S0 signal indicates to the lagging train that the line from Station A
    to Station B is clear.*/

    //  single  red : 1, green:0, n yellow: 2 + n-1

    Block blockOne;
    Block blockTwo;
    Block blockThree;
    Block blockFour;
    Station stationOne;
    Station stationTwo;
    Station stationThree;

    @Before
    public void setUP() {
        // Train At block Three
        stationOne = new Station("A");
        stationTwo = new Station("B");
        stationThree = new Station("C");

        blockOne = new Block(stationOne, stationTwo, "B1",
                false, new Signal());

        stationOne.setNextBlock(blockOne);


        blockTwo = new Block(stationOne, stationTwo,
                blockOne, "B2", false, new Signal());

        blockOne.setNextBlock(blockTwo);


        blockThree = new Block(stationOne, stationTwo,
                blockTwo, "B3", false, new Signal());

        blockTwo.setNextBlock(blockThree);

        blockFour = new Block(stationTwo, stationThree,
                blockThree, "B4", false, new Signal());


        blockThree.setNextBlock(blockFour);

        stationTwo.setNextBlock(blockFour);

        blockOne.markBlockAsUnOccupied();
        //move train to block 2
        blockOne.markNextBlockAsOccupied();
        //move train to block 3
        blockTwo.markNextBlockAsOccupied();


    }


    @Test
    public void testTrainCrossNextStation() {

        //move train to block 4 and cross station
        blockThree.markNextBlockAsOccupied();

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //single s1
        assertEquals(this.blockTwo.getBlockSignalStatus(),
                Integer.valueOf(DOUBLEYELLOW.ordinal()));

        //single s2
        assertEquals(this.blockThree.getBlockSignalStatus(),
                Integer.valueOf(YELLOW.ordinal()));

        //block B1
        Assert.assertEquals(this.blockTwo.getOccupied(), false);

        //block B0
        Assert.assertEquals(this.blockOne.getOccupied(), false);

        //block B2
        Assert.assertEquals(this.blockThree.getOccupied(), false);

        Assert.assertEquals(this.stationOne.IsNextStationSafeToTravel(), true);

        Assert.assertEquals(this.stationTwo.IsNextStationSafeToTravel(), false);

        Assert.assertEquals(this.blockFour.getNextStation().getStationCode(), "C");
        Assert.assertEquals(this.blockThree.getNextStation().getStationCode(), "B");

    }


    @Test
    public void testTrainNotCrossNextStation() {
        Assert.assertEquals(this.stationOne.IsNextStationSafeToTravel(), false);

        Assert.assertEquals(this.stationTwo.IsNextStationSafeToTravel(), true);
    }


}
