package com.oogway.trainsignallings;

import com.oogway.trainsignallingsystem.models.Block;
import com.oogway.trainsignallingsystem.models.Signal;
import com.oogway.trainsignallingsystem.models.Station;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.oogway.trainsignallingsystem.models.Signal.signals.*;
import static junit.framework.TestCase.assertEquals;

public class TestChangeRailwayTopology {

   /* Story 4:
    The first train exits Station B. Thus it exits Block B2.
    This should turn:
    • Signal S4 to Red
    • Signal S3 to Yellow
    • Signal S2 to Yellow Double
    • Signal S1 to Green
    • Signal S0 to Green.
    The Green S0 signal indicates to the lagging train that the line from Station A
    to Station B is clear.*/

        //  single  red : 1, green:0, n yellow: 2 + n-1

        Block blockOne;
        Block blockTwo;
        Block blockThree;
        Block blockFour;
        Block blockFive;
        Station stationOne;
        Station stationTwo;
        Station stationThree;

        @Before
        public void setUP() {
            // Train At block Three
            stationOne = new Station("A");
            stationTwo = new Station("B");
            stationThree = new Station("C");

            blockOne = new Block(stationOne, stationTwo, "B1",
                    false, new Signal());

            stationOne.setNextBlock(blockOne);


            blockTwo = new Block(stationOne, stationTwo,
                    blockOne, "B2", false, new Signal());

            blockOne.setNextBlock(blockTwo);


            blockThree = new Block(stationOne, stationTwo,
                    blockTwo, "B3", false, new Signal());

            blockTwo.setNextBlock(blockThree);

            blockFour = new Block(stationOne, stationTwo,
                    blockThree, "B4", false, new Signal());

            blockThree.setNextBlock(blockFour);

            blockFive = new Block(stationOne, stationTwo,
                    blockFour, "B5", false, new Signal());



//            stationTwo.setNextBlock(blockFour);

            blockFour.setNextBlock(blockFive);

            blockOne.markBlockAsUnOccupied();
            //move train to block 2
            blockOne.markNextBlockAsOccupied();
            //move train to block 3
            blockTwo.markNextBlockAsOccupied();
            blockThree.markNextBlockAsOccupied();


        }


    @Test
    public void testTrainCrossNextStation() {

        //move train to block 4 and cross station

        blockFour.markNextBlockAsOccupied();

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //single s1
        assertEquals(this.blockTwo.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //single s2
        assertEquals(this.blockThree.getBlockSignalStatus(),
                Integer.valueOf(DOUBLEYELLOW.ordinal()));


        //single s3
        assertEquals(this.blockFour.getBlockSignalStatus(),
                Integer.valueOf(YELLOW.ordinal()));

        //single s4
        assertEquals(this.blockFive.getBlockSignalStatus(),
                Integer.valueOf(RED.ordinal()));

        //block B1
        Assert.assertEquals(this.blockTwo.getOccupied(), false);

        //block B0
        Assert.assertEquals(this.blockOne.getOccupied(), false);

        //block B2
        Assert.assertEquals(this.blockThree.getOccupied(), false);

        Assert.assertEquals(this.stationOne.IsNextStationSafeToTravel(), true);


    }
}
