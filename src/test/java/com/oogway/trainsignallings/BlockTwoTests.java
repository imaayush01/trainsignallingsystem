package com.oogway.trainsignallings;

import com.oogway.trainsignallingsystem.models.Block;
import com.oogway.trainsignallingsystem.models.Signal;
import com.oogway.trainsignallingsystem.models.Station;
import org.junit.Before;
import org.junit.Test;

import static com.oogway.trainsignallingsystem.models.Signal.signals.*;
import static org.junit.Assert.assertEquals;

public class BlockTwoTests {
      /*  Story 2:
        The first train moves from S1 to S2. Thus, it exits Block B0 and now occupies Block B1.
        This should turn:
        • Signal S1 to Red
        • Signal S0 to Yellow.
        The Yellow S0 signal will indicate to the lagging train that the
     next block (B0 in this case) is free and it can proceed further to that block
     with caution.*/


    //  single  red : 1, green:0, n yellow: 2 + n-1
    Block blockOne;
    Block blockTwo;

    @Before
    public void setUp() {
        // Train at block One
        Station stationOne = new Station("A");
        Station stationTwo = new Station("B");

        blockOne = blockOne = new Block(stationOne, stationTwo, "B1",
                false, new Signal());

        stationOne.setNextBlock(blockOne);


        blockTwo = new Block(stationOne, stationTwo,
                blockOne, "B2", false,
                new Signal());

        blockOne.setNextBlock(blockTwo);
        blockOne.markBlockAsOccupied();


    }

    @Test
    public void testTrainAtBlockTwo() {
        //move train to block 2
        blockOne.markNextBlockAsOccupied();

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(YELLOW.ordinal()));

        //signal s1
        assertEquals(this.blockTwo.getBlockSignalStatus(),
                Integer.valueOf(RED.ordinal()));

        //signal s2

        //train is on block B1
        assertEquals(this.blockTwo.getOccupied(), true);

        // train is not on Block B0
        assertEquals(this.blockOne.getOccupied(), false);
    }

    @Test
    public void testTrainIsNotAtBlockTwo() {

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(RED.ordinal()));

        //signal s1
        assertEquals(this.blockTwo.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //train is on block B1
        assertEquals(this.blockTwo.getOccupied(), false);

        // train is not on Block B0
        assertEquals(this.blockOne.getOccupied(), true);

    }
}
