package com.oogway.trainsignallings;

import com.oogway.trainsignallingsystem.models.Block;
import com.oogway.trainsignallingsystem.models.Signal;
import com.oogway.trainsignallingsystem.models.Station;
import org.junit.Before;
import org.junit.Test;

import static com.oogway.trainsignallingsystem.models.Signal.signals.GREEN;
import static com.oogway.trainsignallingsystem.models.Signal.signals.RED;
import static org.junit.Assert.assertEquals;

public class BlockOneTests {
    /*Story 1:
        The first train moves from S0 to S1. Thus, it now occupies Block B0.
        This should turn Signal S0 to Red.
        The Red S0 signal will indicate to the lagging train that it has to wait till
        Block B0 is free.free */

    //  single  red : 1, green:0, n yellow: 2 + n-1
    Block blockOne;

    @Before
    public void setUp() {
        Station stationOne = new Station("A");
        Station stationTwo = new Station("B");
        blockOne = new Block(stationOne, stationTwo, "B1",
                false, new Signal());

    }

    @Test
    public void testWhenTrainAtBlockOne() {
        blockOne.markBlockAsOccupied();

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(RED.ordinal()));

        //block one is occupied
        assertEquals(this.blockOne.getOccupied(), true);

    }


    @Test
    public void testWhenTrainIsNotAtBlockOne() {

        //signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //block one is not occupied
        assertEquals(this.blockOne.getOccupied(), false);

    }
}
