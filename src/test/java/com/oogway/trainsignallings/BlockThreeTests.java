package com.oogway.trainsignallings;

import com.oogway.trainsignallingsystem.models.Block;
import com.oogway.trainsignallingsystem.models.Signal;
import com.oogway.trainsignallingsystem.models.Station;
import org.junit.Before;
import org.junit.Test;

import static com.oogway.trainsignallingsystem.models.Signal.signals.*;
import static org.junit.Assert.assertEquals;


public class BlockThreeTests {
    /* Story 3:
        The first train moves from S2 to S3. Thus it exits Block B1 and now occupies Block B2.
        This should turn:
                • Signal S2 to Red
        • Signal S1 to Yellow
        • Signal S0 to Double Yellow.
        The Double Yellow S0 signal will indicate to the lagging train that the next 2
        blocks (B0 and B1) are free and it can safely traverse 2 blocks.*/

    //  single  red : 1, green: 0, n yellow: 2 + n-1

    Block blockOne;
    Block blockTwo;
    Block blockThree;

    @Before
    public void setUP() {
        //train at block two
        Station stationOne = new Station("A");
        Station stationTwo = new Station("B");

        blockOne = blockOne = new Block(stationOne, stationTwo, "B1",
                false, new Signal());

        stationOne.setNextBlock(blockOne);


        blockTwo = new Block(stationOne, stationTwo,
                blockOne, "B2", false,
                new Signal());

        blockOne.setNextBlock(blockTwo);


        blockOne.markNextBlockAsOccupied();


        blockThree = new Block(stationOne, stationTwo,
                blockTwo, "B3", false,
                new Signal());

        blockTwo.setNextBlock(blockThree);


    }

    @Test
    public void testWhenTrainAtBlockThree() {

        //move train to block three
        blockTwo.markNextBlockAsOccupied();


        // signal s0
        assertEquals(this.blockOne.getBlockSignalStatus(),
                Integer.valueOf(DOUBLEYELLOW.ordinal()));


        // signal s1
        assertEquals(this.blockTwo.getBlockSignalStatus(),
                Integer.valueOf(YELLOW.ordinal()));


        // signal s2
        assertEquals(this.blockThree.getBlockSignalStatus(),
                Integer.valueOf(RED.ordinal()));


        // Block B1 is not occupied
        assertEquals(this.blockTwo.getOccupied(), false);

        // Block B0 is not occupied
        assertEquals(this.blockOne.getOccupied(), false);

        //BLock B2 is occupied
        assertEquals(this.blockThree.getOccupied(), true);
    }

    @Test
    public void testWhenTrainIsNotAtBlockThree() {


        // signal s2
        assertEquals(this.blockThree.getBlockSignalStatus(),
                Integer.valueOf(GREEN.ordinal()));

        //BLock B2 not is occupied
        assertEquals(this.blockThree.getOccupied(), false);

    }

}
