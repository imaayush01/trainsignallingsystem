package com.oogway.trainsignallingsystem.models;


public class Station extends Base{
    private String stationCode;

    public Station(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getStationCode() {
        return stationCode;
    }

    public Boolean IsNextStationSafeToTravel() {
        return this.getNextBlock().getStartSignal().getStatus().equals(Signal.signals.GREEN.ordinal());
    }

    public void markStationAsSafe() {
        this.getNextBlock().markBlockAsGreen();
    }

    public String getNextBlockCode() {
        return this.getNextBlock().getBlockCode();
    }


}
