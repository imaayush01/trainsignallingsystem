package com.oogway.trainsignallingsystem.models;


public class Signal {

    //  single  red : 1, green:0, n yellow: 2 + n-1

    private Integer status;

    public Signal() {
        status = signals.GREEN.ordinal();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setStatusAsEnum(Enum status) {
        this.status = status.ordinal();
    }

    public enum signals {
        GREEN(0),
        RED(1),
        YELLOW(2),
        DOUBLEYELLOW(3);

        signals(int i) {
        }
    }

    public void updateSignal(){
        if(this.status>=3){
            this.status = 0;
        }else if(this.status>0 && this.status<3) {
            ++this.status;
        }
    }

}
