package com.oogway.trainsignallingsystem.models;

import static com.oogway.trainsignallingsystem.models.Signal.signals.*;

public class SignalManagement {
    Block blockOne;
    Block blockTwo;
    Block blockThree;
    Block blockFour;
    Station stationOne;
    Station stationTwo;
    Station stationThree;

    public static void main(String[] args) {
        SignalManagement signalManagement = new SignalManagement();
        signalManagement.addBlockSignalStation();
        System.out.println(GREEN.toString() + ": " + GREEN.ordinal());
        System.out.println(RED.toString() + ": " + RED.ordinal());
        System.out.println(YELLOW.toString() + ": " + YELLOW.ordinal());
        System.out.println(DOUBLEYELLOW.toString() + ": " + DOUBLEYELLOW.ordinal());

        signalManagement.blockOne.markBlockAsOccupied();
        signalManagement.store1();

        signalManagement.blockOne.markNextBlockAsOccupied();
        signalManagement.store2();

        signalManagement.blockTwo.markNextBlockAsOccupied();
        signalManagement.store3();

        signalManagement.blockThree.markNextBlockAsOccupied();
        signalManagement.store4();

    }

    public void addBlockSignalStation() {
        stationOne = new Station("A");
        stationTwo = new Station("B");
        stationThree = new Station("C");

        blockOne = new Block(stationOne, stationTwo,
                "B1", false,
                new Signal());

        stationOne.setNextBlock(blockOne);

        blockTwo = new Block(stationOne, stationTwo,
                blockOne, "B2", false,
                new Signal());
        blockOne.setNextBlock(blockTwo);


        blockThree = new Block(stationOne, stationTwo,
                blockTwo, "B3", false,
                new Signal());

        blockTwo.setNextBlock(blockThree);

        blockFour = new Block(stationTwo, stationThree,
                blockThree, "B4", false, new Signal());

        stationTwo.setNextBlock(blockFour);

        blockThree.setNextBlock(blockFour);

    }

    public void store1() {
        System.out.println("Train is at block One");
        System.out.println("S0 :" + this.blockOne.getBlockSignalStatus());

    }

    public void store2() {
        System.out.println("Train is at block two");
        System.out.println("S0: " + this.blockOne.getBlockSignalStatus());
        System.out.println("S1: " + this.blockTwo.getBlockSignalStatus());

    }

    public void store3() {
        System.out.println("Train is at block three");
        System.out.println("S0: " + this.blockOne.getBlockSignalStatus());
        System.out.println("S1: " + this.blockTwo.getBlockSignalStatus());
        System.out.println("S2: " + this.blockThree.getBlockSignalStatus());

    }

    public void store4() {
        System.out.println("Train cross next station");
        System.out.println("S0: " + this.blockOne.getBlockSignalStatus());
        System.out.println("S1: " + this.blockTwo.getBlockSignalStatus());
        System.out.println("S2: " + this.blockThree.getBlockSignalStatus());
        System.out.println("s3 : " + this.blockFour.getBlockSignalStatus());

    }
}
