package com.oogway.trainsignallingsystem.models;

public class Base {
    private String name;
    private Block nextBlock;
    private Station nextStation;
    private Station previousStation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Block getNextBlock() {
        return nextBlock;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }

    public Station getNextStation() {
        return nextStation;
    }

    public Station getPreviousStation() {
        return previousStation;
    }

    public void setPreviousStation(Station previousStation) {
        this.previousStation = previousStation;
    }

    public void setNextStation(Station nextStation) {
        this.nextStation = nextStation;
    }
}
