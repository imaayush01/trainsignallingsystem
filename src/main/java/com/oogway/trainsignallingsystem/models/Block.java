package com.oogway.trainsignallingsystem.models;


import static com.oogway.trainsignallingsystem.models.Signal.signals.*;

public class Block extends Base{

    private Signal startSignal;
    private Block previousBlock;
    private Boolean isOccupied;
    private String blockCode;



    public Block(Station previousStation, Station nextStation, String blockCode,
                 Boolean isOccupied, Signal startSignal) {
        this.setPreviousStation(previousStation);
        this.setNextStation(nextStation);
        this.blockCode = blockCode;
        this.isOccupied = isOccupied;
        this.startSignal = startSignal;

    }

    public Block(Station previousStation, Station nextStation,
                 Block previousBlock, String blockCode, Boolean isOccupied,
                 Signal startSignal) {
        this(previousStation, nextStation, blockCode, isOccupied, startSignal);
        this.previousBlock = previousBlock;
    }


    public Block getPreviousBlock() {
        return previousBlock;
    }

    public Boolean getOccupied() {
        return isOccupied;
    }

    public void setOccupied(Boolean occupied) {
        isOccupied = occupied;
    }

    public void markBlockAsOccupied() {
        this.startSignal.setStatusAsEnum(RED);
        this.isOccupied = true;
    }

    public void markBlockAsUnOccupied() {
        this.startSignal.setStatusAsEnum(YELLOW);
        this.isOccupied = false;
    }


    public Block markNextBlockAsOccupied() {

        this.getNextBlock().markBlockAsOccupied();
        this.markBlockAsUnOccupied();
        this.changePreviousBlocksAsTrainMoveNextBlock();
        return this.getNextBlock();
    }

    public Boolean IsTrainCrossNextStation() {
        Boolean crossNextStation = false;
        if (!this.getPreviousStationCode().equals(
                this.getNextBlock().getPreviousStationCode())) {
            crossNextStation = true;
        }
        return crossNextStation;
    }

    public String getPreviousStationCode() {
        return this.getPreviousStation().getStationCode();
    }

    public void changePreviousBlocksAsTrainMoveNextBlock() {
        Block current = this.previousBlock;

        while (!IsFirstBlockOfStation(current)) {
            current.updateBlockSignalStatus();
            current = current.previousBlock;
        }

        if (this.IsTrainCrossNextStation()) {
            this.markStationSafe();
        } else if (current != null) {
            current.updateBlockSignalStatus();
        }

    }
    public void updateBlockSignalStatus(){
        this.getStartSignal().updateSignal();
    }
    public Boolean IsFirstBlockOfStation(Block currentBlock) {
        if (currentBlock == null) {
            return true;
        }
        return currentBlock.getPreviousStation().getNextBlockCode() == currentBlock.getBlockCode();
    }
    public void markStationSafe() {
        this.getPreviousStation().markStationAsSafe();
    }
    public void markBlockAsGreen() {
        this.updateBlockSignalStatus();
    }

    public Integer getBlockSignalStatus() {
        return this.getStartSignal().getStatus();
    }

    public String getBlockCode() {
        return blockCode;
    }

    public Signal getStartSignal() {
        return startSignal;
    }
}

